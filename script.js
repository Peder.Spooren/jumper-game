window.addEventListener('load', function() {
    const canvas = document.getElementById('canvas1');
    const ctx = canvas.getContext('2d');
    canvas.width = 600;
    canvas.height = 900;
    let platforms = [];

    // Handles input
    class InputHandler {
        constructor() {
            this.keys = [];
            window.addEventListener('keydown', e => {
                if((    e.key === 'ArrowDown' || 
                        e.key === 'ArrowUp' || 
                        e.key === 'ArrowLeft' || 
                        e.key === 'ArrowRight')
                        && this.keys.indexOf(e.key) === -1) {
                    this.keys.push(e.key);
                }
            });

            window.addEventListener('keyup', e => {
                if(     e.key === 'ArrowDown' || 
                        e.key === 'ArrowUp' || 
                        e.key === 'ArrowLeft' || 
                        e.key === 'ArrowRight') {
                    this.keys.splice(e.key.indexOf(e.key), 1);
                }
            });
        }
    }

    class Player {
        constructor(gameWidth, gameHeight) {
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.width = 32;
            this.height = 32;
            this.x = (this.gameWidth / 2) - (this.width / 2);
            this.y = this.gameHeight - this.height;
            this.image = document.getElementById("playerRightImage");
            this.frameX = 0;
            // frameY = 0 -> jump/fall
            // = 1 -> running
            // = 2 -> idle
            this.frameY = 2;
            this.speed = 0;
            this.vy = 0;
            this.gravity = 0.1;
            this.onPlatform = false;
        }

        draw(context) {
            //context.fillRect(this.x, this.y, this.width, this.height);
            context.strokeStyle = 'green';
            context.strokeRect(this.x, this.y, this.width, this.height);
            context.drawImage(this.image, this.frameX * this.width, this.frameY * this.height, this.width, this.height, this.x, this.y, this.width, this.height);
        }

        update(input, platforms) {
            // collision detection
            platforms.forEach(platform => {
                // Need to fix so if the player jumps up to a platform below, that the player doesn't 
                // get put on the platform, but rather bump and go down. 
                if(this.x < platform.x + platform.width && 
                   this.x + this.width > platform.x && 
                   this.y < platform.y + platform.height && 
                   this.y + this.height > platform.y) {
                    this.y = platform.y + this.height;
                    this.vy = 7;
                }
                if(this.x < platform.x + platform.width && 
                   this.x + this.width > platform.x && 
                   this.y >= platform.y - this.height &&
                   this.y <= platform.y + this.height) {
                    this.onPlatform = true;
                    this.y = platform.y - this.height;
                    this.vy = platform.speed;
                }
            });

            // controls
            if(input.keys.indexOf('ArrowRight') > -1) {
                this.image = document.getElementById("playerRightImage");
                this.speed = 3;
            }
            else if(input.keys.indexOf('ArrowLeft') > -1) {
                this.image = document.getElementById("playerLeftImage");
                this.speed = -3;
            }
            else if(input.keys.indexOf('ArrowUp') > -1 && (this.onGround() || this.onPlatform)) {
                this.vy -= 7;
                this.onPlatform = false;
            }
            else {
                this.speed = 0;
            }

            // horizontal movement
            this.x += this.speed;
            

            // vertical movement
            this.y += this.vy;
            if(!this.onGround()) {
                this.vy += this.gravity;
                this.frameY = 0;
                if(this.onPlatform){
                    this.frameY = 2;
                }
            }
            else {
                this.vy = 0;
                this.frameY = 2;
            }
            // game boundaries (walls)
            if(this.x < 0) 
                this.x = 0;
            else if(this.x > this.gameWidth - this.width)
                this.x = this.gameWidth - this.width;
            if(this.y > this.gameHeight - this.height)
                this.y = this.gameHeight - this.height;

        }

        onGround() {
            return this.y >= this.gameHeight - this.height;
        }

    }

    class Background {
        constructor(gameWidth, gameHeight) {
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.image = document.getElementById("backgroundImage");
            this.x = 0;
            this.y = 0;
            this.width = 600;
            this.height = 1800;
            this.speed = 0.3;
        }

        draw(context) {
            context.drawImage(this.image, this.x, this.y, this.width, this.height);
            context.drawImage(this.image, this.x, (this.y - this.height) + 7, this.width, this.height);

        }

        update() {
            // Scrolling background
            this.y += this.speed;
            if(this.y > (this.height / 2))
                this.y = 0;
        }

    }

    class Platform {
        constructor(gameWidth, gameHeight) {
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            // For testing collision
            //this.x = 100;
            this.x = Math.random() * 350;
            this.y = 0;
            // For testing collision
            //this.width = 100;
            this.width = Math.random() * 100 + 100;
            this.height = 8;
            this.image = document.getElementById("platformImage");
            this.speed = 0.8;
            this.markedForDeletion = false;
        }

        draw(context) {
            //context.fillRect(this.x, this.y, this.width, this.height);
            context.strokeStyle = 'red';
            context.strokeRect(this.x, this.y, this.width, this.height);
            context.drawImage(this.image, this.x, this.y, this.width, this.height);
        }

        update() {
            // Platform movement
            this.y += this.speed;
            if(this.y > 900) {
                this.markedForDeletion = true;
            }
        }
    }

    
    // Generates platforms in given randomized time intervall
    function generatePlatforms(deltaTime) {
        if(platformTimer > randomPlatformInterval + platformInterval) {
            platforms.push(new Platform(canvas.width, canvas.height));
            randomPlatformInterval = Math.random() * 1000 + 250;
            platformTimer = 0;
        }
        else {
            platformTimer += deltaTime;
        }

        platforms.forEach(platform => {
            platform.draw(ctx);
            platform.update();
        });

        platforms = platforms.filter(function(platform) {
            return !platform.markedForDeletion;
        });
    }


    function displayStatusText() {

    }

    

    const input = new InputHandler();
    const player = new Player(canvas.width, canvas.height);
    const background = new Background(canvas.width, canvas.height);

    // Numbers used for platform generation
    let lastTime = 0;
    let platformTimer = 0;
    let platformInterval = 1000;
    let randomPlatformInterval = Math.random() * 1000 + 250;

    function animate(timeStamp) {
        const deltaTime = timeStamp - lastTime;
        lastTime = timeStamp;

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        background.draw(ctx);
        background.update();
        
        player.draw(ctx);
        player.update(input, platforms);

        generatePlatforms(deltaTime);

        requestAnimationFrame(animate);

    }

    animate(0);
    
});